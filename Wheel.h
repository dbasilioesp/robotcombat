#pragma once
#include "item.h"
class Wheel :
	public Item
{
public:
	Wheel(void);
	~Wheel(void);

	void setMovement(int movement);
	int getMovement();

	void saveInFileText(ofstream *file);
	void loadFromFileText(ifstream *file);

protected:

	int movement;

};

