#include "Robot.h"


Robot::Robot(void)
{
	hp = 30;
	attack = 1;
	damage = 1;
	range = 1;
	defense = 1;
	movement = 1;
}


Robot::~Robot(void)
{
}


void Robot::setId(int _id)
{
	id = _id;
}

int Robot::getId()
{
	return id;
}

void Robot::setName(string _name)
{
	name = _name;
}

string Robot::getName()
{
	return name;
}

void Robot::addWeapon(Weapon _weapon)
{
	attack += _weapon.getAttack();
	damage += _weapon.getDamage();
	range += _weapon.getRange();

	equips.push_back(_weapon);
}


void Robot::addArmor(Armor _armor)
{
	defense += _armor.getDefense();
	equips.push_back(_armor);
}

void Robot::addWheel(Wheel wheel)
{
	movement += wheel.getMovement();
	equips.push_back(wheel);
}

void Robot::setEquips(vector<Item> _inventory)
{
	equips = _inventory;
}

vector<Item> * Robot::getEquips()
{
	return &equips;
}


void Robot::saveInFileText(ofstream *file)
{
	*file << id << " ";
	*file << name << " ";
	
	// Save the equips [type, id]
	// Ex: attack 1 attack 3 armor 1 wheel 5
	*file << equips.size() << " ";
	for(int i = 0; i < equips.size(); i++)
	{
		*file << equips[i].getType() << " ";
		*file << equips[i].getId() << " ";
	}
}


void Robot::loadFromFileText(ifstream *file)
{
	*file >> id;
	*file >> name;

	// !! The equips must be loaded out of this method !!
}