#include "Game.h"


Game::Game(void)
{
}


Game::~Game(void)
{
}


void Game::initialize(void)
{
	window = new sf::RenderWindow(sf::VideoMode(800, 600), "SFML works!");
    shape = new sf::CircleShape(100.f);
    shape->setFillColor(sf::Color::Green);
}


void Game::play(void)
{
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();
		}

		window->clear();
		window->draw(*shape);
		window->display();
	}
}