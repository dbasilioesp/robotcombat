#include "Weapon.h"


Weapon::Weapon(void)
{
	type = "Weapon";
}


Weapon::~Weapon(void)
{
}


int Weapon::getAttack()
{
	return attack;
}


int Weapon::getDamage()
{
	return damage;
}


int Weapon::getRange()
{
	return range;
}

void Weapon::setAttack(int _attack)
{
	attack = _attack;
}


void Weapon::setDamage(int _damage)
{
	damage = _damage;
}


void Weapon::setRange(int _range)
{
	range = _range;
}

void Weapon::saveInFileText(ofstream *file)
{
	// Chama a fun��o da classe mae para n�o repetir codigo
	Item::saveInFileText(file);

	*file << attack << " ";
	*file << damage << " ";
	*file << range;
}


void Weapon::loadFromFileText(ifstream *file)
{
	Item::loadFromFileText(file);

	*file >> attack;
	*file >> damage;
	*file >> range;
}