#pragma once
#include <vector>
#include "Weapon.h"
#include "Armor.h"
#include "Wheel.h"
#include "Robot.h"

class Loader
{
public:
	Loader(void);
	~Loader(void);

	void load();
	Weapon findWeaponById(int id);
	
protected:

	vector<Robot> robots;
	vector<Weapon> weapons;
	vector<Armor> armors;
	vector<Wheel> wheels;

	void loadWeaponsFromText();
	void loadArmorsFromText();
	void loadWheelsFromText();
	void loadRobotsFromText();
};

