#include "Wheel.h"


Wheel::Wheel(void)
{
	type = "Wheel";
}


Wheel::~Wheel(void)
{
}


void Wheel::setMovement(int _movement)
{
	movement = _movement;
}


int Wheel::getMovement()
{
	return movement;
}


void Wheel::saveInFileText(ofstream *file)
{
	Item::saveInFileText(file);
	*file << movement;
}


void Wheel::loadFromFileText(ifstream *file)
{
	Item::loadFromFileText(file);
	*file >> movement;
}