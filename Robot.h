#pragma once
#include <vector>
#include "Weapon.h"
#include "Armor.h"
#include "Wheel.h"

class Robot
{
public:
	Robot(void);
	~Robot(void);

	void setId(int id);
	int getId();

	void setName(string name);
	string getName();

	void setEquips(vector<Item> equips);
	vector<Item> * getEquips();

	void addWeapon(Weapon weapon);
	void addArmor(Armor armor);
	void addWheel(Wheel wheel);

	void saveInFileText(ofstream *file);
	void loadFromFileText(ifstream *file);
	
protected:
	
	vector<Item> equips;

	int id;
	int hp;
	string name;
	int defense;
	int attack;
	int damage;
	int range;
	int movement;
};