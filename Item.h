#pragma once
#include <iostream>
#include <string>
#include <fstream>
#define ATTACK "ATTACK"

using namespace std;


class Item
{
public:
	Item(void);
	~Item(void);
	
	int getId();
	int getValue();
	string getName();
	string getType();

	void setId(int id);
	void setName(string nome);
	void setValue(int value);
	void setType(int type);

	void saveInFileText(ofstream *file);
	void loadFromFileText(ifstream *file);

protected:
	int id;
	string name;
	string type;
	int value;

};
