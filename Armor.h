#pragma once
#include "item.h"

class Armor :
	public Item
{
public:
	Armor(void);
	~Armor(void);

	void setDefense(int defense);
	int getDefense();

	void saveInFileText(ofstream *file);
	void loadFromFileText(ifstream *file);

protected:
	int defense;

};

