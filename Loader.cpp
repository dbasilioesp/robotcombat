#include "Loader.h"


Loader::Loader(void)
{
}


Loader::~Loader(void)
{
}


void Loader::load()
{
	loadWeaponsFromText();
	loadArmorsFromText();
	loadWheelsFromText();
	loadRobotsFromText();

	for(int i = 0; i < robots.size(); i++)
	{
		cout << robots[i].getName() << endl;

		vector<Item> * itens = robots[i].getEquips();

		for(int j = 0; j < itens->size(); j++)
		{
			if(itens->at(j).getType() == "Weapon")
			{
				Weapon * weapon = (Weapon*) &itens->at(j);
				cout << weapon->getName() << " ";
				cout << weapon->getAttack() << " ";
			}
		}

		cout << endl;
	}
}


void Loader::loadWeaponsFromText()
{
	ifstream file("weapons.txt");
	
	if(!file){
		cout << "File not found!" << endl;
	}else{

		while(!file.eof())
		{
			Weapon weapon;
			weapon.loadFromFileText(&file);
			weapons.push_back(weapon);
		}

		file.close();
	}
}


void Loader::loadArmorsFromText()
{
	ifstream file("armors.txt");
	Armor *armor;

	if(!file){
		cout << "File not found!" << endl;
	}else{

		while(!file.eof())
		{
			armor = new Armor();
			armor->loadFromFileText(&file);
			armors.push_back(*armor);
		}

		file.close();
	}
}


void Loader::loadWheelsFromText()
{
	ifstream file("wheels.txt");
	Wheel *wheel;

	if(!file){
		cout << "File not found!" << endl;
	}else{

		while(!file.eof())
		{
			wheel = new Wheel();
			wheel->loadFromFileText(&file);
			wheels.push_back(*wheel);
		}

		file.close();
	}
}


void Loader::loadRobotsFromText()
{
	ifstream file("robots.txt");
	Robot *robot;

	if(!file){
		cout << "File not found!" << endl;
	}else{

		string type;
		int equips_size;
		int item_id;

		while(!file.eof())
		{	
			robot = new Robot();

			robot->loadFromFileText(&file);
			
			file >> equips_size;

			for(int i = 0; i < equips_size; i++)
			{
				file >> type;
				file >> item_id;

				if(type == "Weapon"){
					Weapon weapon;
					weapon = findWeaponById(item_id);
					robot->addWeapon(weapon);
				}
			}

			// TODO: how to make this better ?
			robots.push_back(*robot);
		}

		file.close();
	}
}


Weapon Loader::findWeaponById(int id)
{
	for(int i = 0; i < weapons.size(); i++){
		if(weapons[i].getId() == id){
			return weapons[i];
		}
	}

}


//void Loader::saveWeaponsInText(vector<Weapon> *weapons)
//{
//	ofstream file("weapons.txt");
//
//	if(!file.is_open()){
//		cout << "File not found" << endl;
//	} else {
//
//		for (int i = 0; i < weapons->size(); i++){
//			weapons->at(i).saveInFileText(&file);
//
//			if(weapons->size() < i - 1){
//				file << endl;
//			}
//		}
//
//		file.close();
//	}
//}