#include "Armor.h"


Armor::Armor(void)
{
	type = "Armor";
}


Armor::~Armor(void)
{
}


void Armor::setDefense(int _defense)
{
	defense = _defense;
}

int Armor::getDefense()
{
	return defense;
}


void Armor::saveInFileText(ofstream *file)
{
	// Chama a fun��o da classe mae para n�o repetir codigo
	Item::saveInFileText(file);

	*file << defense;
}


void Armor::loadFromFileText(ifstream *file)
{
	Item::loadFromFileText(file);

	*file >> defense;
}