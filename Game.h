#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <SFML/Graphics.hpp>
#include "Weapon.h"


using namespace std;


class Game
{

public:
	Game(void);
	~Game(void);

	void initialize();
	void play();
	void finalize();

	void sceneMenu();
	void scenePlaying();
	void sceneGameOver();
	void resetGame();
	void stopGame();

	void drawMenu();
	void drawPlaying();
	void drawGameOver();

	void loadCards();
	
	bool initDelay();
	bool initDelay(int time);

	void saveWeaponsInText(vector<Weapon> *weapons);
	vector<Weapon> * loadWeaponsFromText();

protected:

	//fonte london_font, minicraft_font, digimon_font;
	sf::RenderWindow * window;
	sf::CircleShape * shape;

	char buffer[33];
	string strFonte;

};
